#!/bin/bash

ln -sf /usr/share/zoneinfo/America/Bahia /etc/localtime
hwclock --systohc
sed -i '393s/.//' /etc/locale.gen
locale-gen
echo "LANG=pt_BR.UTF-8" >> /etc/locale.conf
echo "KEYMAP=br-abnt2" >> /etc/vconsole.conf
echo "arch-virtual" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch-virtual" >> /etc/hosts
echo root:r2d2c3po | chpasswd

# You can add xorg to the installation packages, I usually add it at the DE or WM install script
# You can remove the tlp package if you are installing on a desktop or vm

pacman -S grub networkmanager network-manager-applet dialog mtools dosfstools reflector base-devel linux-headers avahi xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils alsa-utils pulseaudio bash-completion openssh rsync reflector acpi acpi_call virt-manager qemu qemu-arch-extra edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat ipset sof-firmware nss-mdns acpid ntfs-3g terminus-font dhcp dhcpcd --needed --noconfirm

# pacman -S --noconfirm xf86-video-amdgpu
# pacman -S --noconfirm --needed xf86-video-intel
# pacman -S --noconfirm --needed xf86-video-qxl
# pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

grub-install --target=i386-pc /dev/sda # replace sdx with your disk name, not the partition
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
#systemctl enable bluetooth
#systemctl enable cups.service
systemctl enable sshd
#systemctl enable avahi-daemon
#systemctl enable tlp # You can comment this command out if you didn't install tlp, see above
systemctl enable reflector.timer
#systemctl enable fstrim.timer
systemctl enable libvirtd
#systemctl enable firewalld
#systemctl enable acpid
systemctl enable dhcpcd

useradd -m -g users -G wheel fnogueira
echo fnogueira:r2d2c3po | chpasswd
usermod -aG libvirt fnogueira

echo "fnogueira ALL=(ALL) ALL" >> /etc/sudoers.d/fnogueira


printf "\e[1;32mPronto! Digite exit, umount -a e reinicie.\e[0m"




