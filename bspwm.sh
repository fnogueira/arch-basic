#!/bin/bash

#sudo timedatectl set-ntp true
#sudo hwclock --systohc

sudo reflector -c Brazil -a 12 --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Syy

#sudo firewall-cmd --add-port=1025-65535/tcp --permanent
#sudo firewall-cmd --add-port=1025-65535/udp --permanent
#sudo firewall-cmd --reload
# sudo virsh net-autostart default

git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si --noconfirm

paru -Syy

#pikaur -S --noconfirm lightdm-slick-greeter
#pikaur -S --noconfirm lightdm-settings
#pikaur -S --noconfirm polybar
#pikaur -S --noconfirm nerd-fonts-iosevka
#pikaur -S --noconfirm ttf-icomoon-feather

paru -S lightdm-settings lightdm-slick-greeter polybar nerd-fonts-noto-sans-mono noto-fonts-main nerd-fonts-droid-sans-mono ttf-icomoon-feather ttf-material-design-icons ttf-material-icons-git siji-git nerd-fonts-jetbrains-mono lf lsd zplug nerd-fonts-iosevka zsh zsh-you-should-use zsh-fast-syntax-highlighting zsh-history-substring-search zsh-autosuggestions zsh-completions --noconfirm --needed

#pikaur -S --noconfirm system76-power
#sudo systemctl nable --now system76-power
#sudo system76-power graphics integrated
#pikaur -S --noconfirm gnome-shell-extension-system76-power-git
#pikaur -S --noconfirm auto-cpufreq
#sudo systemctl enable --now auto-cpufreq

echo "PACOTES PRINCIPAIS"

sudo pacman -Syy

sleep 5

sudo pacman -S --noconfirm --needed bspwm sxhkd picom nitrogen dmenu alacritty arandr git rofi archlinux-wallpaper neofetch python-pywal thunar thunar-archive-plugin xclip pacman-contrib dunst scrot lxappearance xfce4-settings wget unzip nodejs npm python3 python-pip python-pynvim pkgfile fd ripgrep xorg light-locker lightdm rxvt-unicode nautilus simplescreenrecorder alsa-utils pulseaudio alsa-utils pulseaudio-alsa pavucontrol arc-gtk-theme arc-icon-theme dina-font tamsyn-font bdf-unifont ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid gnu-free-fonts ttf-ibm-plex ttf-liberation ttf-linux-libertine ttf-roboto tex-gyre-fonts ttf-ubuntu-font-family ttf-anonymous-pro ttf-cascadia-code ttf-fantasque-sans-mono ttf-fira-mono ttf-hack ttf-fira-code ttf-inconsolata ttf-jetbrains-mono ttf-monofur adobe-source-code-pro-fonts cantarell-fonts inter-font ttf-opensans gentium-plus-font ttf-junicode adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts noto-fonts-cjk noto-fonts-emoji ttf-font-awesome awesome-terminal-fonts playerctl neovim ttf-iosevka-nerd ttf-fantasque-sans-mono otf-fantasque-sans-mono terminus-font lightdm-gtk-greeter

#sudo flatpak install -y spotify
#sudo flatpak install -y kdenlive

sudo systemctl enable lightdm

mkdir -p .config/{bspwm,sxhkd,dunst}

install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc
echo 'setxkbmap -model abnt2 -layout br -variant abnt2' >> ~/.config/bspwm/bspwmrc


git config --global user.email "fnogueira@protonmail.com"
git config --global user.name "Fábio Nogueira"

sleep 5
printf "\e[1;32mALTERE OS ARQUIVOS NECESSÁRIOS ANTES DE REINICIAR!\e[0m"

